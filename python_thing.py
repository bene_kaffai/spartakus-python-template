#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# by: bene
#

import paho.mqtt.client as mqtt
import json


##############################################################################
# GLOABAL VARIABLES
##############################################################################

MQTTP_HOST = '192.168.0.100'  # '192.168.2.110'  # "192.128.0.1"
MQTTP_PORT = 1883

CLIENTID = 'TEMPLATE'

NETWORK = '/spartakus/thing/'
TOPIC_OUT = NETWORK + CLIENTID + '/output/'
TOPIC_IN = NETWORK + CLIENTID + '/input/'
SUBSCRIBE_THINGS = NETWORK + '+/output/#'
TOPIC_PING = NETWORK + 'all/input/ping'

# Json with all I/O infos about this node
REPORT_IO = json.dumps({'name': CLIENTID, 'status': 1, 'type': 'software',
                        'input': {'brightness': 1, 'volume': 1, 'screen': 1, 'report': 1}, 'output': {'brightness': 1, 'report': 'json'}})


##############################################################################
# HELPER FUNCTIONS
##############################################################################

# send MQTT messages to logic
def send_mqtt(topic, payload):
    client.publish(topic, payload)


# returns matching Dictonary Key (callback) to topic
def get_matching(topic):
    for key in mqtt_callback.keys():
        if topic.startswith(key):
            return key


# parse topic for sender
def get_thing(topic):
    return topic.split(NETWORK)[1].split('/')[0]
    # return filter(None, msg.topic.split('/'))
    # return [x for x in msg.topic.split('/') if x]


# parse topic for argument
def get_argument(topic):
    return topic.split(NETWORK)[1].split('/')[-1]


##############################################################################
# MQTT CALLBACK
##############################################################################

# default callback if no specific topic is registered
def callback_default(thing, argument, message):
    print("default_msg: " + argument + " " + str(message) + "\n")


def callback_screen(thing, argument, message):
    # do input screen stuff here
    print("callback_screen: " + argument + " " + str(message) + "\n")


def callback_volume(thing, argument, message):
    print("callback_volume: " + argument + " " + str(message) + "\n")


def callback_brightness(thing, argument, message):
    print('callback_brightness %s' % message)
    os.system('xbacklight -set %s' % message)
    send_mqtt(TOPIC_OUT + 'brightness', message)


# send infos about own in- and outnodes
def callback_report(thing, argument, message):
    print('callback_report on: %s' % message)
    send_mqtt(TOPIC_OUT + 'report', REPORT_IO)  # send report info on first connection to broker

# register all callbacks for different topics of Parent and Child things
mqtt_callback = {TOPIC_IN + 'report': callback_report,
                 TOPIC_PING: callback_report,
                 TOPIC_IN + 'screen': callback_screen,
                 TOPIC_IN + 'brightness': callback_brightness,
                 TOPIC_IN + 'volume': callback_volume}


def on_connect(client, userdata, flags, rc):
    client.subscribe(TOPIC_IN + '#')  # subscribe to all incoming topics
    client.subscribe(TOPIC_PING)  # subscribe to global ping
    send_mqtt(TOPIC_IN + 'report', 'send report on connect')


def on_message(client, userdata, msg):
    thing = get_thing(msg.topic)
    argument = get_argument(msg.topic)
    try:
        message = float(msg.payload.decode("utf-8"))
    except ValueError:
        message = msg.payload.decode("utf-8")
    callback = mqtt_callback.get(get_matching(msg.topic), callback_default)
    callback(thing, argument, message)

client = mqtt.Client(CLIENTID)
client.on_connect = on_connect  # register callback for event
client.on_message = on_message  # register callback for event

client.connect(MQTTP_HOST, MQTTP_PORT, 60)


##############################################################################
# MAIN LOOP
##############################################################################

try:
    while True:
        client.loop()
        # time.sleep(0.1)
except KeyboardInterrupt:
    client.publish('/spartakus/thing/LOGIC/input/thing/remove', CLIENTID)
    print('interrupted!')
print('bye bye!')
