# README Spartakus Python template #


### SYSTEM ###

Auf Basis eines Protokolls auf MQTT Basis sprechen diverse Entitäten in einem Netzwerk miteinander. Entitäten können hier sowohl Hardwareobjekte sein (WEMOS/ARDUINO Microcontroller mit angeschlossenen Sensoren/Aktoren), fertige IoT Lösungen (Philips HUE, etc.), oder Softwareanwendung (Twitter, Python Skripte, Processing Sketche, Telegram Messenger, etc.). Diese Entitäten sind als Knotenpunkte (Nodes) gedacht. Ihre In- und Outputs können miteinander verknüpft werden.


### Python Template ###

Python 3 Script zur Erzeugung von eigenen Python Entitäten im Spartakus Netzwerk.
Alle nötigen Funktionen und JSONs um eigene Entitäten entsprechend dem [Spartakus MQTT Protokoll](https://bitbucket.org/bene_kaffai/spartakus-logic/raw/3f78f37546dc13350222d92efcb8bb45450847ec/web%20of%20entities%20prototyp.pdf) zu schreiben.  


### How do I get set up? ###

* Python 3 installieren
* Python Dependencies installieren: paho.mqtt.client, json ``` pip3 install paho-mqtt rethinkdb```
* einen beliebigen MQTT-Broker im Netzwerk starten ``` Defaults: MQTTP_HOST = '192.168.0.100' MQTTP_PORT = 1883```
* RethinkDB installieren und starten ``` Defaults DB_HOST = 'localhost' DB_PORT = 28015```
* ``` python3 logic.py ``` aus  [Logic](https://bitbucket.org/bene_kaffai/spartakus-logic/overview) starten
* ``` python3 python_thing.py ``` starten


### Weitere Tools für das Spartakus Framework ###

* [Logic](https://bitbucket.org/bene_kaffai/spartakus-logic/overview) (Kommunikationszentrale des Systems)
* [View](https://bitbucket.org/bene_kaffai/spartakus-view) (grafische Nutzer*Innen-Oberfläche zum nodebasierten erstellen und verwalten von Interkationen zwischen Entitäten)
* [Logic Tings](https://bitbucket.org/bene_kaffai/spartakus-logic-thing) (einfache Logikbausteine AND, OR, COMPARE, etc. um Entitäten komplex miteinander zu verknüpfen)
* [WEMOS](https://bitbucket.org/bene_kaffai/spartakus-wemos-template) Template (um eigene Entitäten im Netzwerk verfügbar zu machen)


### Who do I talk to? ###
* MIT
* Version 0.1
* GUI für das Spartakus Framework als Beispiel für die CeBit 2017
* Repo owner or admin: bene_kaffai, https://bitbucket.org/MaxDemian/, https://bitbucket.org/BenHatscher/
